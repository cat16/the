use super::types::*;

pub fn tau(m: f32) -> Rad {
    cgmath::Rad(std::f32::consts::PI * 2.0 * m)
}
