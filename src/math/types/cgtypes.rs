use cgmath;

pub type Vec3f = cgmath::Vector3<f32>;
pub type Vec3u = cgmath::Vector3<u32>;
pub type Pt3 = cgmath::Point3<f32>;
pub type Mat4 = cgmath::Matrix4<f32>;
pub type RawMat4 = [[f32;4];4];
pub type Quat = cgmath::Quaternion<f32>;
pub type Rad = cgmath::Rad<f32>;
pub use cgmath::Rotation3 as Rot3;
pub use cgmath::Rotation as Rot;
pub use cgmath::Decomposed as Decomp;
pub use cgmath::Transform;
pub use cgmath::Matrix;
