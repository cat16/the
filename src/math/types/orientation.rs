use super::cgtypes::*;

pub const RIGHT: Vec3f = Vec3f::new(1.0, 0.0, 0.0);
pub const UP: Vec3f = Vec3f::new(0.0, 1.0, 0.0);
pub const FORWARD: Vec3f = Vec3f::new(0.0, 0.0, 1.0);

pub struct Orientation {
    quat: Quat,
}

impl Orientation {
    pub fn direction(&self, dir: Vec3f) -> Vec3f {
        self.quat.conjugate() * dir
    }

    pub fn pitch(&mut self, angle: Rad) {
        self.rotate(Quat::from_axis_angle(RIGHT, angle));
    }

    pub fn yaw(&mut self, angle: Rad) {
        self.rotate(Quat::from_axis_angle(UP, angle));
    }

    pub fn roll(&mut self, angle: Rad) {
        self.rotate(Quat::from_axis_angle(FORWARD, angle));
    }

    pub fn rotate(&mut self, rotation: Quat) {
        self.quat = rotation * self.quat;
    }

    pub fn mat(&self) -> Mat4 {
        self.quat.into()
    }
}

impl Default for Orientation {
    fn default() -> Self {
        Self {
            quat: Quat::new(1.0, 0.0, 0.0, 0.0)
        }
    }
}
