use crate::math::types; use types::*;
pub use types::Orientation;

pub struct Position(pub Vec3f);

impl Position {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Self(Vec3f::new(x, y, z))
    }
}

pub struct Scale(pub Vec3f);

impl Scale {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Self(Vec3f::new(x, y, z))
    }
}

impl Default for Scale {
    fn default() -> Self {
        Self(Vec3f::new(1.0, 1.0, 1.0))
    }
}
