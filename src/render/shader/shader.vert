#version 450

layout(location=0) in vec3 position;
layout(location=1) in vec3 normal;

layout(location=0) out vec3 v_position;
layout(location=1) out vec3 v_normal;

layout(set=0, binding=0) uniform Camera {
    mat4 perspective;
    mat4 view;
} camera;

layout(set=1, binding=0) uniform Transform {
    mat4 translation;
    mat4 orientation;
    mat4 scale;
} transform;

void main() {
    mat4 transform_mat = transform.translation * transform.orientation * transform.scale;
    v_position = vec3(transform_mat * vec4(position, 1.0));
    v_normal = mat3(transpose(inverse(transform_mat))) * normal;
    gl_Position = camera.perspective * camera.view * transform_mat * vec4(position, 1.0);
}
