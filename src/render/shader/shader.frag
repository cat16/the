#version 450

layout(location=1) in vec3 v_normal;

layout(location=0) out vec4 color;

layout(set=1, binding=1) uniform Material {
    vec3 color_in;
};

vec3 LIGHT = vec3(-0.2, -0.8, 0.1);

void main() {
    float lum = max(dot(normalize(v_normal), normalize(LIGHT)), 0.0);
    color = vec4(
        (0.3 + 0.7 * lum) * color_in,
        1.0
    );
}
