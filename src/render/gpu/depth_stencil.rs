use super::*;

pub fn create_texture_view(device: &Device, width: u32, height: u32) -> TextureView {
    device.create_texture(&TextureDescriptor {
        size: Extent3d {
            width: width,
            height: height,
            depth: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: TextureDimension::D2,
        format: TextureFormat::Depth32Float,
        usage: TextureUsage::OUTPUT_ATTACHMENT,
        label: None,
    }).create_default_view()
}

pub fn attachment(view: &TextureView) -> Option<RenderPassDepthStencilAttachmentDescriptor> {
    Some(wgpu::RenderPassDepthStencilAttachmentDescriptor {
        attachment: view,
        depth_ops: Some(wgpu::Operations {
            load: wgpu::LoadOp::Clear(1.0),
            store: true,
        }),
        stencil_ops: None
    })
}

pub fn state() -> Option<DepthStencilStateDescriptor> {
    Some(wgpu::DepthStencilStateDescriptor {
        format: wgpu::TextureFormat::Depth32Float,
        depth_write_enabled: true,
        depth_compare: wgpu::CompareFunction::Less,
        stencil_front: wgpu::StencilStateFaceDescriptor::IGNORE,
        stencil_back: wgpu::StencilStateFaceDescriptor::IGNORE,
        stencil_read_mask: 0,
        stencil_write_mask: 0,
    })
}
