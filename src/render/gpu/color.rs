use super::*;

pub fn states(sc_desc: &SwapChainDescriptor) -> [ColorStateDescriptor; 1] {
    [wgpu::ColorStateDescriptor {
        format: sc_desc.format,
        color_blend: wgpu::BlendDescriptor::REPLACE,
        alpha_blend: wgpu::BlendDescriptor::REPLACE,
        write_mask: wgpu::ColorWrite::ALL,
    }]
}

pub fn attachments<'a>(view: &'a TextureView) -> [RenderPassColorAttachmentDescriptor<'a>; 1] {
    [wgpu::RenderPassColorAttachmentDescriptor {
        attachment: view,
        resolve_target: None,
        ops: wgpu::Operations {
            load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
            store: true,
        },
    }]
}
