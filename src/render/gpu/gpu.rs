use super::*;

pub async fn setup(window: &winit::window::Window) -> GPUInterface {
    let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
    let surface = unsafe { instance.create_surface(window) };

    let adapter = instance.request_adapter(
        &wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::Default,
            compatible_surface: Some(&surface),
        },
        wgpu::UnsafeExtensions::disallow(),
    ).await.expect("failed to get an adapter");

    let (device, queue) = adapter.request_device(
        &wgpu::DeviceDescriptor {
            extensions: wgpu::Extensions::empty(),
            limits: wgpu::Limits::default(),
            shader_validation: true,
        },
        None
    ).await.expect("failed to get a device");

    let size = window.inner_size();

    let sc_desc = wgpu::SwapChainDescriptor {
        usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
        format: wgpu::TextureFormat::Bgra8UnormSrgb,
        width: size.width,
        height: size.height,
        present_mode: wgpu::PresentMode::Mailbox,
    };

    let swap_chain = device.create_swap_chain(&surface, &sc_desc);

    GPUInterface {surface, device, queue, sc_desc, swap_chain}
}

pub struct GPUInterface {
    pub surface: Surface,
    pub device: Device,
    pub queue: Queue,
    pub sc_desc: SwapChainDescriptor,
    pub swap_chain: SwapChain,
}
