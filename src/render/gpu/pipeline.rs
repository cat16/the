use super::*;
use super::super::mesh;

pub fn setup(device: &Device, sc_desc: &SwapChainDescriptor, bind_group_layouts: &[&BindGroupLayout]) -> RenderPipeline {

    let layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        bind_group_layouts
    });

    let vs_module = device.create_shader_module(wgpu::include_spirv!("../shader/shader.vert.spv"));
    let fs_module = device.create_shader_module(wgpu::include_spirv!("../shader/shader.frag.spv"));

    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        layout: &layout,
        vertex_stage: wgpu::ProgrammableStageDescriptor {
            module: &vs_module,
            entry_point: "main",
        },
        fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
            module: &fs_module,
            entry_point: "main",
        }),
        rasterization_state: Some(wgpu::RasterizationStateDescriptor {
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: wgpu::CullMode::Back,
            depth_bias: 0,
            depth_bias_slope_scale: 0.0,
            depth_bias_clamp: 0.0,
        }),
        primitive_topology: wgpu::PrimitiveTopology::TriangleList,
        color_states: &color::states(&sc_desc),
        depth_stencil_state: depth_stencil::state(),
        vertex_state: wgpu::VertexStateDescriptor {
            index_format: wgpu::IndexFormat::Uint32,
            vertex_buffers: &[mesh::Vertex::desc()],
        },
        sample_count: 1,
        sample_mask: !0,
        alpha_to_coverage_enabled: false,
    })
}
