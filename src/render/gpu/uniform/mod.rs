mod util;

pub mod global;
pub mod mesh;

use crate::math::types::*;
use std::mem;
use wgpu::*;
use util::*;
