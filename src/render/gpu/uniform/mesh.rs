use super::*;

pub struct Uniforms {
    pub bind_group: UniformBindGroup,
    pub transform_buf: DynBuf<Transform>,
    pub material_buf: DynBuf<Material>
}

impl Uniforms {
    pub fn new(device: &Device, layouts: &mut Vec<BindGroupLayout>) -> Self {
        let transform_buf = DynBuf::new(&device, 3);
        let material_buf = DynBuf::new(&device, 3);
        let bind_group = UniformBindGroupBuilder::new(device, 1)
            .with_dyn(&transform_buf, ShaderStage::VERTEX)
            .with_dyn(&material_buf, ShaderStage::FRAGMENT)
            .build(layouts);
        Self {
            bind_group,
            transform_buf,
            material_buf
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct Transform {
    pub translation: RawMat4,
    pub orientation: RawMat4,
    pub scale: RawMat4
}

unsafe impl bytemuck::Pod for Transform {}
unsafe impl bytemuck::Zeroable for Transform {}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct Material {
    pub color: [f32;3]
}

unsafe impl bytemuck::Pod for Material {}
unsafe impl bytemuck::Zeroable for Material {}
