use super::*;
use std::marker::PhantomData;
use bytemuck::Pod;

// sets

pub struct UniformBindGroup {
    pub index: u32,
    pub bind_group: BindGroup,
    offsets: Vec<u32>
}

impl UniformBindGroup {
    pub fn offsets_at(&self, index: usize) -> Vec<u32> {
        self.offsets.iter().map(|o| o * index as u32).collect()
    }
}

pub struct UniformBindGroupBuilder<'a> {
    device: &'a Device,
    index: u32,
    bindings: Vec<Binding<'a>>,
    binding_entries: Vec<BindGroupLayoutEntry>,
    offsets: Vec<u32>
}

impl <'a> UniformBindGroupBuilder<'a> {
    pub fn new(device: &'a Device, index: u32) -> Self {
        Self {
            device,
            index,
            bindings: Vec::new(),
            binding_entries: Vec::new(),
            offsets: Vec::new()
        }
    }

    pub fn with<Data : Pod>(mut self, buf: &'a Buf<Data>, stage: ShaderStage) -> Self {
        self.binding_entries.push(BindGroupLayoutEntry::new(
            self.binding_entries.len() as u32,
            stage,
            BindingType::UniformBuffer {
                dynamic: false,
                min_binding_size: BufferSize::new(mem::size_of::<Data>() as u64),
            },
        ));
        self.bindings.push(Binding {
            binding: self.bindings.len() as u32,
            resource: BindingResource::Buffer(buf.buf.slice(..))
        });
        self
    }

    pub fn with_dyn<Data : Pod>(mut self, buf: &'a DynBuf<Data>, stage: ShaderStage) -> Self {
        self.binding_entries.push(BindGroupLayoutEntry::new(
            self.binding_entries.len() as u32,
            stage,
            BindingType::UniformBuffer {
                dynamic: true,
                min_binding_size: BufferSize::new(buf.size),
            },
        ));
        self.bindings.push(Binding {
            binding: self.bindings.len() as u32,
            resource: BindingResource::Buffer(buf.buf.slice(..))
        });
        self.offsets.push(buf.size as u32);
        self
    }

    pub fn build(self, layouts: &'a mut Vec<BindGroupLayout>) -> UniformBindGroup {
        let layout = self.device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: None,
            bindings: &self.binding_entries
        });
        let bind_group = self.device.create_bind_group(&BindGroupDescriptor {
            layout: &layout,
            bindings: &self.bindings,
            label: None,
        });
        layouts.push(layout);
        UniformBindGroup {
            index: self.index,
            bind_group,
            offsets: self.offsets
        }
    }
}

// buffers

pub struct Buf<Data : Pod> {
    buf: Buffer,
    phantom: PhantomData<Data>
}

impl <Data : Pod> Buf<Data> {
    pub fn new(device: &Device) -> Self {
        Self {
            buf: device.create_buffer(&BufferDescriptor {
                label: None,
                size: mem::size_of::<Data>() as u64,
                usage: BufferUsage::UNIFORM | BufferUsage::COPY_DST,
                mapped_at_creation: false
            }),
            phantom: PhantomData
        }
    }

    pub fn write_to(&self, queue: &Queue, data: &Data) {
        queue.write_buffer(
            &self.buf, 0, bytemuck::bytes_of(data)
        );
    }
}

pub struct DynBuf<Data : Pod> {
    buf: Buffer,
    size: u64,
    count: u64,
    phantom: PhantomData<Data>
}

impl <Data : Pod> DynBuf<Data> {
    pub fn new(device: &Device, start_count: u64) -> Self {
        let data_size = mem::size_of::<Data>() as u64;
        let size = BIND_BUFFER_ALIGNMENT * (data_size / BIND_BUFFER_ALIGNMENT) + BIND_BUFFER_ALIGNMENT;
        Self {
            buf: device.create_buffer(&BufferDescriptor {
                label: None,
                size: size * start_count,
                usage: BufferUsage::UNIFORM | BufferUsage::COPY_DST,
                mapped_at_creation: false
            }),
            size,
            count: start_count,
            phantom: PhantomData
        }
    }

    pub fn write_to(&self, queue: &Queue, index: usize, data: &Data) {
        queue.write_buffer(
            &self.buf, self.size * index as u64, bytemuck::bytes_of(data)
        );
    }
}
