use super::*;

pub struct Uniforms {
    pub bind_group: UniformBindGroup,
    pub camera_buf: Buf<Camera>
}

impl Uniforms {
    pub fn new(device: &Device, layouts: &mut Vec<BindGroupLayout>) -> Self {
        let camera_buf = Buf::new(device);
        let bind_group = UniformBindGroupBuilder::new(device, 0)
            .with(&camera_buf, ShaderStage::VERTEX)
            .build(layouts);
        Self {
            bind_group,
            camera_buf
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct Camera {
    pub perspective: RawMat4,
    pub view: RawMat4
}

unsafe impl bytemuck::Pod for Camera {}
unsafe impl bytemuck::Zeroable for Camera {}
