use winit::window::{Window, WindowBuilder};
use super::*;

pub struct Renderer {
    pub window: Window,
    pub mesh_pool: MeshPool,

    pub interface: gpu::GPUInterface,
    pub render_pipeline: wgpu::RenderPipeline,
    pub global_uniforms: uniform::global::Uniforms,
    pub mesh_uniforms: uniform::mesh::Uniforms,
    pub depth_view: wgpu::TextureView,
}

impl Renderer {
    pub async fn create(event_loop: &winit::event_loop::EventLoop<()>) -> Self {
        let window = WindowBuilder::new()
            .with_title("the")
            .build(event_loop)
            .unwrap();

        let interface = gpu::setup(&window).await;
        let device = &interface.device;
        let sc_desc = &interface.sc_desc;
        
        let mut layouts = Vec::new();
        let global_uniforms = uniform::global::Uniforms::new(device, &mut layouts);
        let mesh_uniforms = uniform::mesh::Uniforms::new(device, &mut layouts);

        let render_pipeline = pipeline::setup(
            device, sc_desc,
            &layouts.iter().collect::<Vec<&wgpu::BindGroupLayout>>()[..]
        );
        
        let mut mesh_pool = MeshPool::new();
        mesh_pool.load(device);

        let depth_view = depth_stencil::create_texture_view(device, sc_desc.width, sc_desc.height);

        Self { window, mesh_pool, interface, render_pipeline,
            global_uniforms, mesh_uniforms, depth_view }
    }

    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        let interface = &mut self.interface;
        let width = new_size.width;
        let height = new_size.height;
        interface.sc_desc.width = width;
        interface.sc_desc.height = height;
        self.depth_view = depth_stencil::create_texture_view(&interface.device, width, height);
        interface.swap_chain = interface.device.create_swap_chain(&interface.surface, &interface.sc_desc);
    }

    pub fn aspect_ratio(&self) -> f32 {
        let size = self.window.inner_size();
        size.width as f32 / size.height as f32
    }
}
