use wgpu::{Buffer, BufferUsage};
use super::*;

pub struct Mesh {
    pub vertex_buffer: Buffer,
    pub index_buffer: Buffer,
    pub vertex_count: usize,
    pub index_count: usize
}

impl Mesh {
    pub fn create(device: &wgpu::Device, data: &MeshData) -> Self {
        let mut vertices = vec![];
        for i in 0..data.vertices.len() {
            vertices.push(
                Vertex {
                    position: data.vertices[i].into(),
                    normal: data.normals[i].into()
                }
            )
        }

        let indices = &data.indices;
        let vertex_buffer = device.create_buffer_with_data(bytemuck::cast_slice(&vertices), BufferUsage::VERTEX);
        let index_buffer = device.create_buffer_with_data(bytemuck::cast_slice(&indices), BufferUsage::INDEX);

        Self {
            vertex_buffer, index_buffer, index_count: indices.len(), vertex_count: vertices.len()
        }
    }
}

pub struct MeshData {
    pub vertices: Vec<Vec3f>,
    pub normals: Vec<Vec3f>,
    pub indices: Vec<u32>
}

impl MeshData {
    pub fn new(vertices: Vec<Vec3f>, normals: Vec<Vec3f>, indices: Vec<u32>) -> Self {
        Self {vertices, normals, indices}
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Vertex {
    position: [f32; 3],
    normal: [f32; 3],
}

unsafe impl bytemuck::Pod for Vertex {}
unsafe impl bytemuck::Zeroable for Vertex {}

impl Vertex {
    pub fn desc<'a>() -> wgpu::VertexBufferDescriptor<'a> {
        use std::mem;
        wgpu::VertexBufferDescriptor {
            stride: mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::InputStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttributeDescriptor {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float3,
                },
                wgpu::VertexAttributeDescriptor {
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float3,
                },
            ]
        }
    }
}
