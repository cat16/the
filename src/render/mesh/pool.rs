use std::collections::HashMap;
use super::*;

pub struct MeshPool {
    meshes: HashMap<String, Mesh>
}

impl MeshPool {
    pub fn new() -> Self {
        MeshPool {
            meshes: HashMap::new()
        }
    }

    pub fn get(&self, name: &str) -> Option<&Mesh> {
        self.meshes.get(name)
    }

    pub fn load(&mut self, device: &wgpu::Device) {
        self.insert("sword", Mesh::create(&device, &types::gltf_loader::load("models/sword.glb")));
        self.insert("sphere", Mesh::create(&device, &types::icosphere::create(3)));
    }

    pub fn insert(&mut self, key: &str, mesh: Mesh) -> Option<Mesh> {
        self.meshes.insert(key.into(), mesh)
    }
}

pub struct MeshIdentifier(pub String);
impl MeshIdentifier {
    pub fn new(v: &str) -> Self {Self(v.into())}
}
