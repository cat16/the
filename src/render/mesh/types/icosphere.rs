use super::*;
use std::collections::HashMap;

pub fn create(recursion_level: u32) -> MeshData {
    // create 12 vertices of a icosahedron
    let t = (1.0 + 5.0f32.sqrt()) / 2.0;

    let mut vertices: Vec<Vec3f> = Vec::new();
    let mut normals: Vec<Vec3f> = Vec::new();

    let mut index = 0;

    use rand::Rng;
    let mut r = rand::thread_rng();

    let mut vert = |vertices: &mut Vec<Vec3f>, x: f32, y: f32, z: f32| -> u32 {
        // normalize + randomize vector
        let length = (x.powi(2) + y.powi(2) + z.powi(2)).sqrt();
        let mut vert = Vec3f::new(x/length, y/length, z/length);
        vert += Vec3f::new(r.gen(), r.gen(), r.gen()) * 0.1;
        vertices.push(vert);
        // normalize vector for normals, idk if this is correct due to randomizing
        let length = (vert.x.powi(2) + vert.y.powi(2) + vert.z.powi(2)).sqrt();
        normals.push(vert / length);
        // increment & return index
        index+=1;
        index-1
    };

    vert(&mut vertices, -1.0,  t,  0.0);
    vert(&mut vertices,  1.0,  t,  0.0);
    vert(&mut vertices, -1.0, -t,  0.0);
    vert(&mut vertices,  1.0, -t,  0.0);

    vert(&mut vertices,  0.0, -1.0,  t);
    vert(&mut vertices,  0.0,  1.0,  t);
    vert(&mut vertices,  0.0, -1.0, -t);
    vert(&mut vertices,  0.0,  1.0, -t);

    vert(&mut vertices,  t,  0.0, -1.0);
    vert(&mut vertices,  t,  0.0,  1.0);
    vert(&mut vertices, -t,  0.0, -1.0);
    vert(&mut vertices, -t,  0.0,  1.0);

    // create 20 triangles of the icosahedron
    let mut faces: Vec<Vec3u> = Vec::new();

    let mut face = |x, y, z| {
        faces.push(Vec3u::new(x, y, z));
    };

    // 5 faces around point 0
    face(0, 11, 5);
    face(0, 5, 1);
    face(0, 1, 7);
    face(0, 7, 10);
    face(0, 10, 11);

    // 5 adjacent faces 
    face(1, 5, 9);
    face(5, 11, 4);
    face(11, 10, 2);
    face(10, 7, 6);
    face(7, 1, 8);

    // 5 faces around point 3
    face(3, 9, 4);
    face(3, 4, 2);
    face(3, 2, 6);
    face(3, 6, 8);
    face(3, 8, 9);

    // 5 adjacent faces 
    face(4, 9, 5);
    face(2, 4, 11);
    face(6, 2, 10);
    face(8, 6, 7);
    face(9, 8, 1);


    let mut mpi_cache: HashMap<i64, u32> = HashMap::new();


    // return index of point in the middle of p1 and p2
    let mut get_middle_point = |vertices: &mut Vec<Vec3f>, p1: u32, p2: u32| {

        // first check if we have it already
        let first_is_smaller = p1 < p2;
        let smaller_index = if first_is_smaller {p1} else {p2} as i64;
        let greater_index = if first_is_smaller {p2} else {p1} as i64;
        let key = (smaller_index.wrapping_shl(32)) + greater_index;

        if let Some(ret) = mpi_cache.get(&key) {
            return *ret;
        }

        // not in cache, calculate it
        let point1 = vertices[p1 as usize];
        let point2 = vertices[p2 as usize];
        let middle = Vec3f::new(
            (point1.x + point2.x) / 2.0, 
            (point1.y + point2.y) / 2.0, 
            (point1.z + point2.z) / 2.0);

        // add vertex makes sure point is on unit sphere
        let i = vert(vertices, middle.x, middle.y, middle.z); 

        // store it, return index
        mpi_cache.insert(key, i);
        i
    };


    // refine triangles
    for _ in 0..recursion_level {
        let mut faces2: Vec<Vec3u> = Vec::new();

        let mut face2 = |x, y, z| {
            faces2.push(Vec3u::new(x, y, z));
        };

        for tri in faces {
            // replace triangle by 4 triangles
            let a = get_middle_point(&mut vertices, tri.x, tri.y);
            let b = get_middle_point(&mut vertices, tri.y, tri.z);
            let c = get_middle_point(&mut vertices, tri.z, tri.x);

            face2(tri.x, a, c);
            face2(tri.y, b, a);
            face2(tri.z, c, b);
            face2(a, b, c);
        }
        faces = faces2;
    }

    let mut indices: Vec<u32> = Vec::new();

    // done, now add triangles to mesh
    for tri in faces {
        indices.push(tri.x);
        indices.push(tri.y);
        indices.push(tri.z);
    }

    MeshData::new(vertices, normals, indices)
}
