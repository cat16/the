use super::*;

pub fn load(path: &str) -> MeshData {
    let (document, buffers, _images) = gltf::import(path).expect("failed to import gltf");
    let mut vertices = Vec::new();
    let mut normals = Vec::new();
    let mut indices = Vec::new();

    for scene in document.scenes() {
        for node in scene.nodes() {
            let (mut vs, mut ns, mut is) = parse_node(&buffers, node);
            vertices.append(&mut vs);
            normals.append(&mut ns);
            indices.append(&mut is);
        }
    }

    MeshData::new(vertices, normals, indices)
}

fn parse_node(buffers: &Vec<gltf::buffer::Data>, node: gltf::Node) -> (Vec<Vec3f>, Vec<Vec3f>, Vec<u32>) {

    let mat = match node.transform() {
        gltf::scene::Transform::Matrix{matrix} => Mat4::from(matrix),
        gltf::scene::Transform::Decomposed{translation, rotation, scale} => {
            Mat4::from_translation(Vec3f::from(translation))
                * Mat4::from(Quat::from(rotation))
                * Mat4::from_nonuniform_scale(scale[0], scale[1], scale[2])
        }
    };

    let mut vertices = Vec::new();
    let mut normals = Vec::new();
    let mut indices = Vec::new();

    match node.mesh() {
        Some(mesh) => {
            for primitive in mesh.primitives() {
                let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()]));
                if let Some(verts) = reader.read_positions() {
                    vertices.append(&mut verts.map(|v| Vec3f::from(v)).collect());
                }
                if let Some(norms) = reader.read_normals() {
                    normals.append(&mut norms.map(|n| Vec3f::from(n)).collect());
                }
                if let Some(inds) = reader.read_indices() {
                    indices.append(&mut inds.into_u32().collect::<Vec<_>>())
                }
            }
            for i in 0..(vertices.len()) {
                vertices[i] = mat.transform_vector(vertices[i]);
            }
        },
        None => ()
    }

    for child in node.children() {
        let (mut vs, mut ns, mut is) = parse_node(&buffers, child);
        vertices.append(&mut vs.iter_mut().map(|v| mat.transform_vector(*v)).collect());
        normals.append(&mut ns);
        indices.append(&mut is);
    }

    (vertices, normals, indices)
}
