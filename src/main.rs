mod render;
mod math;
mod util;
mod system;
mod resource;
mod component;

use resource::*;
use component::*;

use winit::event_loop::{EventLoop, ControlFlow};
use winit::event::*;
use legion::prelude::Schedule;

async fn run() {
    let event_loop = EventLoop::new();
    
    let universe = legion::world::Universe::new();
    let mut world = universe.create_world();
    world.insert((), vec![
        (Position::new(0.0, 0.0, 5.0), Orientation::default(), crate::render::MeshIdentifier::new("sword"))
    ]);

    world.insert((), vec![
        (Position::new(5.0, 0.0, 5.0), Scale::default(), crate::render::MeshIdentifier::new("sword"))
    ]);

    world.insert((), vec![
        (Position::new(0.0, 0.0, -5.0), crate::render::MeshIdentifier::new("sphere"))
    ]);

    let mut resources = legion::systems::resource::Resources::default();
    let renderer = render::Renderer::create(&event_loop).await;
    let ratio = renderer.aspect_ratio();
    resources.insert(renderer);
    resources.insert(Delta::new());
    resources.insert(Input::new());
    resources.insert(GameState::default());
    resources.insert(Camera::new(cgmath::Deg(90.0).into(), ratio, 0.1, 1024.0));

    let mut schedule = Schedule::builder()
        .add_system(system::player::create_system())
        .add_system(system::test::create_system())
        .add_thread_local_fn(system::render::create_system())
        .build();

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::MainEventsCleared => {
                schedule.execute(&mut world, &mut resources);
                let mut delta = resources.get_mut::<Delta>().unwrap();
                delta.stop();
                delta.start();
                let mut input = resources.get_mut::<Input>().unwrap();
                input.post_update();
            },
            _ => {
                let mut renderer = resources.get_mut::<render::Renderer>().unwrap();
                let mut input_ = resources.get_mut::<Input>().unwrap();
                let mut camera = resources.get_mut::<Camera>().unwrap();
                match event {
                    Event::WindowEvent {event, ..} => {
                        match event {
                            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                            WindowEvent::Resized(size) => {
                                renderer.resize(size);
                                camera.resize(size);
                            },
                            WindowEvent::KeyboardInput {input, ..} => input_.handle_keyboard(input),
                            _ => ()
                        }
                    },
                    Event::DeviceEvent {event, ..} => {
                        input_.handle_device(event);
                    },
                    _ => ()
                }
            }
        }
    })
}

fn main() {
    futures::executor::block_on(run());
}
