use super::*;
use crate::render::*;
use crate::math::types::*;

pub fn create_system() -> Box<dyn FnMut(&mut World, &mut Resources)> {
    let transform_query = <(
        Read<Position>,
        TryRead<Orientation>,
        TryRead<Scale>
    )>::query().filter(
        component::<MeshIdentifier>()
        & (changed::<Position>() | changed::<Orientation>() | changed::<Scale>())
    );

    let render_query = <(
        Read<MeshIdentifier>,
    )>::query().filter(component::<Position>());

    Box::new(move |world: &mut World, resources: &mut Resources| {
        use std::ops::DerefMut;
        let mut reference = resources.get_mut::<Renderer>().unwrap();
        let mut camera = resources.get_mut::<Camera>().unwrap();
        let Renderer{
            interface, global_uniforms, mesh_uniforms, render_pipeline,
            mesh_pool, depth_view, ..
        } = reference.deref_mut();

        let GPUInterface {queue, device, swap_chain, ..} = interface;

        if camera.dirty {
            global_uniforms.camera_buf.write_to(queue, &uniform::global::Camera {
                perspective: camera.perspective().into(),
                view: camera.view().into()
            });

            camera.dirty = false;
        }

        for (i, (pos, orientation, scale)) in transform_query.iter(world).enumerate() {
            mesh_uniforms.transform_buf.write_to(queue, i, &uniform::mesh::Transform {
                translation: Mat4::from_translation(pos.0).into(),
                // I really wanna use .unwrap_or_default() here but the refs mess it up, there's probably another way
                orientation: match orientation {
                    Some(o) => o.mat(),
                    None => Orientation::default().mat()
                }.into(),
                scale: {
                    let s = match scale {
                        Some(s) => s.0,
                        None => Scale::default().0
                    };
                    Mat4::from_nonuniform_scale(s.x, s.y, s.z).into()
                }
            });

            const COLORS: [[f32;3];12] = [
                [1.0, 0.0, 0.0],
                [1.0, 0.5, 0.0],
                [1.0, 1.0, 0.0],
                [0.5, 1.0, 0.0],
                [0.0, 1.0, 0.0],
                [0.0, 1.0, 0.5],
                [0.0, 1.0, 1.0],
                [0.0, 0.5, 1.0],
                [0.0, 0.0, 1.0],
                [0.5, 0.0, 1.0],
                [1.0, 0.0, 1.0],
                [1.0, 0.0, 0.5]
            ];

            mesh_uniforms.material_buf.write_to(queue, i, &uniform::mesh::Material {
                color: COLORS[i%COLORS.len()]
            });
        }

        let frame = swap_chain.get_next_frame()
            .expect("Failed to acquire next swap chain texture")
            .output;

        let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            color_attachments: &color::attachments(&frame.view),
            depth_stencil_attachment: depth_stencil::attachment(&depth_view)
        });
        rpass.set_pipeline(&render_pipeline);

        let bg = &global_uniforms.bind_group;
        rpass.set_bind_group(bg.index, &bg.bind_group, &[]);

        for (i, mesh_id) in render_query.iter(world).enumerate() {
            let mesh = mesh_pool.get(&(mesh_id.0).0);
            if let Some(mesh) = mesh {
                let bg = &mesh_uniforms.bind_group;
                rpass.set_bind_group(bg.index, &bg.bind_group, &bg.offsets_at(i));

                rpass.set_vertex_buffer(0, mesh.vertex_buffer.slice(..));
                rpass.set_index_buffer(mesh.index_buffer.slice(..));
                rpass.draw_indexed(0..mesh.index_count as u32, 0, 0..1);
            }
        }

        drop(rpass);

        queue.submit(Some(encoder.finish()));
    })
}
