use super::*;
use crate::math::types::*;
use winit::event::VirtualKeyCode::*;

pub fn create_system() -> Box<dyn Schedulable> {
    SystemBuilder::new("player")
        .read_resource::<Delta>()
        .write_resource::<Renderer>()
        .read_resource::<Input>()
        .write_resource::<GameState>()
        .write_resource::<Camera>()
        .build(|_, _, (delta, renderer, input, state, camera), _| {
            for key in input.pressed() {
                match key {
                    Escape => {
                        state.movement_controls = !state.movement_controls;
                        renderer.window.set_cursor_visible(!state.movement_controls);
                    },
                    _ => ()
                };
            }

            if state.movement_controls {
                let raw_speed = delta.per_sec(2.0);
                let raw_roll_speed = cgmath::Rad(delta.per_sec(2.0));
                for key in input.down() {
                    match key {
                        W => camera.move_(FORWARD, raw_speed),
                        S => camera.move_(FORWARD, -raw_speed),
                        A => camera.move_(RIGHT, raw_speed),
                        D => camera.move_(RIGHT, -raw_speed),
                        Space => camera.move_(UP, raw_speed),
                        LShift => camera.move_(UP, -raw_speed),
                        Q => camera.orientation.roll(raw_roll_speed),
                        E => camera.orientation.roll(-raw_roll_speed),
                        _ => ()
                    }
                }

                let sensitivity = 100.0;
                let (mdx, mdy) = input.mouse_delta();
                let (xch, ych) = (mdx != 0f64, mdy != 0f64);
                // I'm p sure this can be done cleaner with a switch
                if xch {
                    camera.orientation.yaw(cgmath::Rad(mdx as f32 * sensitivity / 50000.0));
                }
                if ych {
                    camera.orientation.pitch(cgmath::Rad(-mdy as f32 * sensitivity / 50000.0));
                }

                if xch || ych {
                    let size = &renderer.window.inner_size();
                    &renderer.window.set_cursor_position(winit::dpi::PhysicalPosition{
                        x: size.width/2, y: size.height/2
                    });
                }
            }
        })
}
