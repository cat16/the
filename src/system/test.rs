use super::*;

pub fn create_system() -> Box<dyn Schedulable> {
    let query1 = <Write<Orientation>>::query().filter(component::<Position>());
    let query2 = <Write<Scale>>::query().filter(component::<Position>());
    SystemBuilder::new("test")
        .with_query(query1)
        .with_query(query2)
        .read_resource::<Delta>()
        .build(|_, world, delta, (query1, query2)| {
            for mut orientation in query1.iter_mut(world) {
                orientation.roll(cgmath::Rad(delta.per_sec(1.0)));
            }
            for mut scale in query2.iter_mut(world) {
                scale.0.x += delta.per_sec(0.1);
            }
        })
}