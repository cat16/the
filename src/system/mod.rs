use legion::prelude::*;
use crate::resource::*;
use crate::component::*;

pub mod player;
pub mod test;
pub mod render;
