pub struct Wrapper<T>(pub T);

impl <T> From<T> for Wrapper<T> {
    fn from(val: T) -> Self {
        Self {0: val}
    }
}

pub fn swap_remove_item<T : Eq>(vec: &mut Vec<T>, item: &T) -> Option<T> {
    match vec.iter().position(|i| i.eq(item)) {
        Some(index) => Some(vec.swap_remove(index)),
        None => None
    }
}
