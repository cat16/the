use cgmath;

use super::super::math::all::*;

pub struct Camera {
    data: CameraData,
    pub dirty: bool
}

pub struct CameraData {
    pub fov: Rad,
    pub aspect_ratio: f32,
    pub z_near: f32,
    pub z_far: f32,

    pub position: Pt3,
    pub orientation: Orientation
}

impl Camera {
    pub fn new(fov: Rad, aspect_ratio: f32, z_near: f32, z_far: f32) -> Self {
        Self {
            data: CameraData {
                fov,
                aspect_ratio,
                z_near,
                z_far,
                position: Pt3::new(0.0, 0.0, 0.0),
                orientation: {
                    let mut o = Orientation::default();
                    o.roll(tau(0.5));
                    o
                }
            },
            dirty: true
        }
    }

    pub fn perspective(&self) -> Mat4 {
        cgmath::PerspectiveFov {
            fovy: self.fov,
            aspect: self.aspect_ratio,
            near: self.z_near,
            far: self.z_far,
        }.into()
    }

    pub fn view(&self) -> Mat4 {
        Mat4::look_at_dir(
            self.position,
            self.orientation.direction(FORWARD),
            self.orientation.direction(UP)
        )
    }

    pub fn move_(&mut self, direction: Vec3f, amount: f32) {
        let inner = self.deref_mut();
        inner.position += inner.orientation.direction(direction) * amount;
    }

    pub fn resize(&mut self, size: winit::dpi::PhysicalSize<u32>) {
        self.aspect_ratio = size.width as f32 / size.height as f32;
    }
}

use std::ops::{Deref, DerefMut};

impl Deref for Camera {
    type Target = CameraData;
    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl DerefMut for Camera {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.dirty = true;
        &mut self.data
    }
}
