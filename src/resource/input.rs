use crate::util::swap_remove_item;

use winit::event::{
    VirtualKeyCode as Keycode,
    KeyboardInput,
    DeviceEvent,
    ElementState
};

pub struct Input {
    mouse_delta: (f64, f64),
    pressed: Vec<Keycode>,
    down: Vec<Keycode>,
}

impl Input {
    pub fn new() -> Self {
        Self {
            mouse_delta: (0., 0.),
            pressed: Vec::new(),
            down: Vec::new()
        }
    }

    pub fn key_down(&self, key: Keycode) -> bool {
        self.down.contains(&key)
    }

    pub fn down(&self) -> &Vec<Keycode> {
        &self.down
    }

    pub fn pressed(&self) -> &Vec<Keycode> {
        &self.pressed
    }

    pub fn mouse_delta(&self) -> (f64, f64) {
        self.mouse_delta
    }

    pub fn handle_keyboard(&mut self, input: KeyboardInput) {
        if let Some(key) = input.virtual_keycode {
            match input.state {
                ElementState::Pressed => if !self.down.contains(&key) {
                    self.pressed.push(key);
                    self.down.push(key);
                },
                ElementState::Released => {
                    swap_remove_item(&mut self.pressed, &key);
                    swap_remove_item(&mut self.down, &key);
                }
            }
        }
    }

    pub fn handle_device(&mut self, event: DeviceEvent) {
        match event {
            DeviceEvent::MouseMotion {delta} => {
                self.mouse_delta.0 += delta.0;
                self.mouse_delta.1 += delta.1;
            },
            _ => ()
        }
    }

    pub fn post_update(&mut self) {
        self.pressed.clear();
        self.mouse_delta = (0., 0.);
    }
}
