pub struct GameState {
    pub movement_controls: bool
}

impl Default for GameState {
    fn default() -> Self {
        Self {
            movement_controls: false
        }
    }
}
