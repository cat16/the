use std::time::{Instant, Duration};

#[derive(Copy, Clone)]
pub struct Delta {
    delta: Duration,
    now: Instant
}

impl Delta {
    pub fn new() -> Self {
        Self {
            delta: Duration::from_nanos(0),
            now: Instant::now()
        }
    }

    pub fn start(&mut self) {
        self.now = Instant::now();
    }

    pub fn stop(&mut self) {
        self.delta = self.now.elapsed();
    }

    pub fn per_sec(&self, n: f32) -> f32 {
        n * self.delta.as_secs_f32()
    }
}
